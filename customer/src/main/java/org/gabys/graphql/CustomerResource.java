package org.gabys.graphql;

import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.graphql.*;
import org.gabys.entites.Customer;
import org.gabys.repositories.CustomerRepository;

import javax.inject.Inject;
import java.util.List;

@GraphQLApi
public class CustomerResource {

    @Inject
    CustomerRepository repository;
    @Query("allCustomer")
    @Description("Get all customers from database")
    public Uni<List<Customer>> getAllCustomer(){
        return repository.listAll();
    }

    @Query
    @Description("Get a customer from database")
    public  Uni<Customer> getCustomer(@Name("customerId") Long Id){
        return repository.findById(Id);
    }

    @Mutation
    public Uni<Customer> addCustomer(Customer customer){
        return repository.addMutation(customer);
    }
    @Mutation
    public Uni<Boolean> deleteCustomer(Long Id){
        return  repository.deleteMutation(Id);
    }
}
