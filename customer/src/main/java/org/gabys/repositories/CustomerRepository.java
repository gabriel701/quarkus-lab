package org.gabys.repositories;


import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Uni;
import lombok.extern.slf4j.Slf4j;
import org.gabys.entites.Customer;
import org.jboss.resteasy.reactive.RestPath;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Slf4j
@ApplicationScoped
public class CustomerRepository implements PanacheRepositoryBase<Customer, Long> {
    @Inject
    CustomerRepository repository;

    @GET
    public Uni<List<PanacheEntityBase>> list() {

        return Customer.listAll(Sort.by("names"));
    }

    @GET
    @Path("using-repository")
    public Uni<List<Customer>> listUsingRepository() {
        return repository.findAll().list();
    }

    @GET
    @Path("/{Id}")
    public Uni<PanacheEntityBase> getById(@PathParam("Id") Long Id) {
        return Customer.findById(Id);
    }

    @GET
    @Path("/{Id}/product")
    public Uni<Customer> getByIdProduct(@PathParam("Id") Long Id) {
        return repository.getByIdProduct(Id);
    }

    @POST
    public Uni<Response> add(Customer c) {
        return repository.add(c);
    }

    @DELETE
    @Path("/{Id}")
    public Uni<Response> delete(@PathParam("Id") Long Id) {
        return repository.delete(Id);
    }

    @PUT
    @Path("{id}")
    public Uni<Response> update(@RestPath Long id, Customer c) {
        return repository.update(id, c);
    }

    public Uni<Boolean> deleteMutation(Long Id){
        return Panache.withTransaction(() -> Customer.deleteById(Id));
    }
    public Uni<Customer> addMutation(Customer customer){
        customer.getProducts().forEach( p -> p.setCustomer(customer));
        return Panache.withTransaction(customer:: persist)
                .replaceWith(customer);
    }
}
